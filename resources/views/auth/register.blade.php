
@extends('layouts.app')

@section('content')


<style>
    .test22 {
        font-family: 'Bodoni Moda';
font-style: italic;
font-weight: 700;
font-size: 26px;
line-height: 40px;
text-align: center;
color: #777777;
}
</style>
<form id="signUpForm" method="POST" action="{{ route('register') }}">
    @csrf
    <div class="text-center">
    <img src="{{asset('img/logo-register.png') }}" style="width: 200px;" class="mb-5 mt-2" alt="logo">
</div>
    <h5 class="text-center mt-2 mb-5" 
        style="font-family: 'Montserrat';
                font-style: normal;
                font-weight: 400;
                font-size: 22px;
                line-height: 23px;
                text-align: center;
                color: #5A5A5A;
                margin-bottom: -30px !important;
" id="Titulo">Sigue los 2 pasos, crea<br>
                                        tu cuenta y <strong> <i style="font-family: 'Bodoni Moda';
font-style: italic;
font-weight: 700;"> disfruta </i><br>
                                        de los beneficios de Olié.</strong> </h5>
<!--Segundo titulo---->                                        
<h5 class="text-center mt-2 mb-5 Titulo-2" 
        style="font-family: 'Montserrat';
                font-style: normal;
                font-weight: 400;
                font-size: 0.1px;
                line-height: 23px;
                text-align: center;
                color: #5A5A5A;
                "> <strong> <i style="font-family: 'Bodoni Moda';
font-style: italic;
font-weight: 700;"> Solo un paso más</i></strong> </h5>
                            
    <div class="form-header d-flex mb-4">

        <span class="stepIndicator" style="color: #5A5A5A !important;"><strong>Configuración de la cuenta</strong></span>
        <span class="stepIndicator" style="color: #5A5A5A !important;">Datos personales</span>
    </div>
    <!-- end step indicators -->

    <!-- step one -->
    <div class="step">
        <div class="mb-3">
        <input type="text" name="username" style="border: solid 1px #1D1D1B;" class="form-control" placeholder="Username" aria-label="Name" value="{{ old('username') }}">
                                @error('username') <p class='text-danger text-xs pt-1'> {{ $message }} </p> @enderror
        </div>
        <div class="mb-3">
        <input type="email" name="email" style="border: solid 1px #1D1D1B;" class="form-control" placeholder="Email" aria-label="Email" value="{{ old('email') }}">
                                @error('email') <p class='text-danger text-xs pt-1'> {{ $message }} </p> @enderror
        </div>
    </div>

    <!-- step three -->

    <div class="step">
        <p class="text-center mb-4" style="color: #9A9A9A !important;"><i class="fa fa-lock" style="margin-right: 5px; color: #9A9A9A !important;"></i>Protección de datos</p>
        <div class="mb-3">
                                <input type="password" style="border: solid 1px #1D1D1B;" name="password" class="form-control" placeholder="Password" aria-label="Password">
                                @error('password') <p class='text-danger text-xs pt-1'> {{ $message }} </p> @enderror
                           
        </div>
        <div class="mb-3">
            <input id="phone" type="number" style="border: solid 1px #1D1D1B;" placeholder="Telefono" class="form-control" name="phone" value="" required>

        </div>
        <div class="form-check form-check-info text-start">
            <input class="" style="width: auto !important; color:#1D1D1B !important;" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="flexCheckDefault" style="color:#1D1D1B !important;">
                Recordar
            </label>
            @error('terms') <p class='text-danger text-xs'> {{ $message }} </p> @enderror
        </div>
        <div class="form-check form-check-info text-start">
            <input class="" style="width: auto !important; color:#1D1D1B !important;" type="checkbox" name="terms" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault" style="color:#1D1D1B !important;">
            Acepto los <a href="/aviso" class="text-dark font-weight-bolder"  style="color:#1D1D1B !important;">terminos y terminos y condiciones</a>
            </label>
            @error('terms') <p class='text-danger text-xs'> {{ $message }} </p> @enderror
        </div>

    </div>

    <!-- start previous / next buttons -->
    <div class="form-footer d-flex">
        <button type="button" id="prevBtn" onclick="nextPrev(-1)" style="border-radius:0px !important;">ANTERIOR</button>
        <button type="button" id="nextBtn" onclick="nextPrev(1)" style="font-weight: 500; font-size: 21px; border-radius:0px !important;" >SIGUIENTE</button>
    </div>
    <!-- end previous / next buttons -->
    <!--  <button type="submit" class="btn btn-outline-danger btn-lg">
                                        {{ __('Registrar') }}
                                    </button>-->
    <p class="text-xl mt-3 mb-0 cuenta text-center text-dark" style="color:#777777 !important;  ">¿Ya tienes una cuenta? <a href="{{ route('login') }}" class="text-dark font-weight-bolder"  style="color:#777777 !important;  ">Inicia Sesión</a></p>

</form>
<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("step");
       
        
        x[n].style.display = "block";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "ENVIAR";
        } else {
            document.getElementById("nextBtn").innerHTML = "SIGUIENTE";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("step");

        console.log("primer current: " + currentTab);

        document.getElementById("Titulo").innerHTML="<h5 class='test22'>Solo un paso más</h5>"

        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:

        x[currentTab].style.display = "none";
      
        // Increase or decrease the current tab by 1:

        currentTab = currentTab + n;

        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("signUpForm").submit();
            return false;
        }

        // Otherwise, display the correct tab:
        showTab(currentTab);
        }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("step");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("stepIndicator")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    

    function fixStepIndicator(n) {
       
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("stepIndicator");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }


</script>
@include('layouts.footers.guest.footer')
@endsection