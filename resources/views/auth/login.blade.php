@extends('layouts.app')

@section('content')
<style>
    .form-switch .form-check-input:checked {
    border-color: black !important;
    background-color: black !important;
}
.text-gradient.text-primary {
    background-image: linear-gradient(310deg, #000000, #000000) !important;
}
</style>
    <main class="main-content  mt-0">
        <section>
            <div class="page-header min-vh-100">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
                            <div class="card card-plain">
                                <div class="card-header pb-0 text-start">
                                    <h4 class="font-weight-bolder"  style="color: #5A5A5A !important;">Iniciar sesión</h4>
                                    <p class="mb-0" style="color: #5A5A5A !important;">Ingrese su correo electrónico y contraseña para iniciar sesión</p>
                                </div>
                                <div class="card-body">
                                    <form role="form" method="POST" action="{{ route('login.perform') }}">
                                        @csrf
                                        @method('post')
                                        <div class="flex flex-col mb-3">
                                            <input type="email" name="email" class="form-control form-control-lg" value="{{ old('email') ?? 'admin@argon.com' }}" aria-label="Email">
                                            @error('email') <p class="text-danger text-xs pt-1"> {{$message}} </p>@enderror
                                        </div>
                                        <div class="flex flex-col mb-3">
                                            <input type="password" name="password" class="form-control form-control-lg" aria-label="Password" value="secret" >
                                            @error('password') <p class="text-danger text-xs pt-1"> {{$message}} </p>@enderror
                                        </div>
                                        <div class="form-check form-switch">
                                            <input class="form-check-input" name="remember" type="checkbox" id="rememberMe">
                                            <label class="form-check-label" for="rememberMe">Recuérdame</label>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit"  style="background: black !important; border-radius:0px !important;" class="btn btn-lg btn-primary btn-lg w-100 mt-4 mb-0">INICIAR SESIÓN</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                                    <p class="mb-1 text-sm mx-auto">
                                    ¿Olvidaste tu contraseña? Restablece tu contraseña
                                        <a href="{{ route('reset-password') }}" class="text-primary text-gradient font-weight-bold" style="color:black !important;">Aqui</a>
                                    </p>
                                </div>
                                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                                    <p class="mb-4 text-sm mx-auto">
                                    ¿No tienes una cuenta? 
                                        <a href="{{ route('register') }}" class="text-primary text-gradient font-weight-bold" style="color:black !important;">Regístrate</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div
                            class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
                            <div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center overflow-hidden"
                                style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/signin-ill.jpg');
              background-size: cover;">
                                <span class="mask bg-gradient-primary opacity-6"></span>
                                <h4 class="mt-5 text-white font-weight-bolder position-relative">"La atención es la nueva moneda"</h4>
                                <p class="text-white position-relative">Una vez que pruebas Olié, verás que el esfuerzo que hemos puesto en el proceso ha valido la pena.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
