@extends('layouts.app')

@section('content')
<style>
    h1{
        color: black !important;
    }
    h2{
        color: black !important;
    }
    h3{
        color: black !important;
    }
    p{
        color: black !important;
    }
    ul>li{
        color: black !important;
    }
</style>
<main class="container">

    <h1>AVISO DE PRIVACIDAD</h1>
    <h2>Responsable</h2>
    <p>En cumplimiento a la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, Hogar y Spa, Sociedad Anónima de Capital Variable, (en adelante OLIÉ) le informa que OLIÉ es responsables de sus datos personales. El “Usuario” podrá contactar a OLIÉ en cualquier momento a través de nuestro correo electrónico <a href="mailto:hola@olie.mx">hola@olie.mx</a> o directamente en nuestras oficinas ubicadas en Las flores 49, Colonia. Tlalpan centro 1, Delegación Tlalpan, CDMX, Código postal 14000 y nuestro teléfono es 55 2120 6136. Protegemos y salvaguardamos sus datos personales para evitar el daño, pérdida, destrucción, robo, extravío, alteración, así como el tratamiento no autorizado de sus datos personales.</p>
    <p>En Olié nos preocupamos por tu bienestar y también por la seguridad de nuestros clientes, por ello es que a partir del día 1 de marzo del 2021, solicitemos documentos adicionales antes de enviar su pedido, esto con el fin de proteger los intereses de nuestros clientes y evitar fraudes en línea. Con esta información, podremos verificar sus datos y proceder al envío.</p>
    <h2>Datos personales</h2>
    <p>La información deberá ser veraz y completa. El usuario responderá en todo momento por los datos proporcionados y en ningún caso OLIÉ será responsable de la veracidad de los mismos. La información solicitada al usuario en el sitio OLIÉ es:</p>
    <ul>
        <li>Nombre completo</li>
        <li>Edad</li>
        <li>Teléfono</li>
        <li>Dirección Postal</li>
        <li>Correo electrónico</li>
        <li>Datos de Facturación</li>
    </ul>
    <p>Sus datos personales serán tratados con base en los principios de licitud, consentimiento, información, calidad, finalidad, lealtad, proporcionalidad y responsabilidad en términos de la Legislación. Se mantendrá la confidencialidad de sus datos personales estableciendo y manteniendo de forma efectiva las medidas de seguridad administrativas, técnicas y físicas, para evitar su daño, pérdida, alteración, destrucción, uso, acceso o divulgación indebida.</p>
    <h2>Uso de la información OLIÉ</h2>
    <p>Sus datos personales serán utilizados para las siguientes finalidades:</p>
    <ul>
        <li>Proveer los productos, servicios o información requeridos por usted.</li>
        <li>Proveer los productos, servicios o información requeridos por usted.</li>
        <li>Informar sobre cambios y nuevos productos que estén relacionados con el contratado o adquirido por el cliente.</li>
        <li>Dar cumplimiento a obligaciones contraídas con nuestros clientes.</li>
        <li>Evaluar la calidad del servicio.</li>
        <li>Gestión de sorteos, promociones o encuestas.</li>
        <li>Mejora de la efectividad de nuestros sitios web.</li>
        <li>Esfuerzos de mercadotecnia o publicidad.</li>
        <li>Realizar estudios internos sobre hábitos de consumo.</li>
        <li>Los datos personales o empresariales proporcionados por el usuario formarán parte de un archivo que contendrá su perfil.</li>
        <li>El usuario puede modificar su perfil en cualquier momento utilizando su número de usuario y contraseña.</li>
        </ul>
        <h2> Limitación de uso y divulgación de información </h2>
        <p>En nuestro programa de notificación de promociones, ofertas y servicios a través de correo electrónico, sólo OLIÉ tiene acceso a la información recabada. Este tipo de publicidad se realiza mediante avisos y mensajes promocionales de correo electrónico, los cuales sólo serán enviados a usted y a aquellos contactos registrados para tal propósito, esta indicación podrá usted modificarla en cualquier momento enviando un correo a hola@olie.mx En los correos electrónicos enviados, pueden incluirse ocasionalmente ofertas de terceras partes que sean nuestros socios comerciales. Usted puede cancelar la suscripción a los boletines de correo electrónico haciendo clic en el botón siguiente a la leyenda “Cancelar Suscripción” en la parte inferior de los correos electrónicos enviados por OLIÉ La frecuencia de envíos de los boletines de información de OLIÉ puede ser de hasta dos correos semanales.</p>
<h2>Derechos ARCO (acceso, rectificación, cancelación y oposición) </h2>
<p>El área responsable del manejo y la administración de los datos personales es: Centro de Atención a Clientes a quien puede contactar mediante el correo electrónico hola@olie.mx o directamente en Las flores 49, Colonia. Tlalpan centro 1, Delegación Tlalpan, CDMX, Código postal 14000 y nuestro teléfono es 55 2120 6136.
¿Cómo ejercer sus Derechos ARCO o revocar su consentimiento para el tratamiento de dichos datos? Usted tiene derecho de (i) acceder a sus datos personales que poseemos y a los detalles del tratamiento de los mismos; (ii) rectificarlos en caso de ser inexactos o incompletos; (iii) cancelarlos cuando considere que no se requieren para alguna de las finalidades señalados en el presente aviso de privacidad, estén siendo utilizados para finalidades no consentidas o haya finalizado la relación contractual; (iv) oponerse al tratamiento de los mismos para fines específicos; o (v) revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, a fin de que dejemos de hacer uso de los mismos. Los mecanismos que se han implementado para el ejercicio de dichos derechos son a través de la presentación de la solicitud respectiva en: hola@olie.mx Su solicitud deberá contener al menos la siguiente información: (i) nombre completo y correo electrónico o domicilio para comunicarle la respuesta a su solicitud; (ii) los documentos que acrediten su identidad o, en su caso, la representación legal; (iii) la descripción clara y precisa de los datos personales respecto de los que busca ejercer alguno de los derechos antes mencionados; y (iv) cualquier otro elemento o documento que facilite la localización de los datos personales.
De conformidad con lo dispuesto por la Ley Federal de Protección de Datos Personales en Posesión de los Particulares,OLIÉ cuenta con un plazo de veinte días hábiles, contados desde la fecha en que se recibió la solicitud, para comunicarle la determinación adoptada y, en caso de resultar procedente, OLIÉ deberá hacerla efectiva dentro de los quince días hábiles siguientes a la fecha en que haya comunicado la respuesta.
Los plazos antes referidos podrán ser ampliados una sola vez por un periodo igual, siempre y cuando así lo justifiquen las circunstancias del caso. La obligación de acceso a la información se dará por cumplida cuando se pongan a disposición los datos personales mediante copias simples o documentos electrónicos. OLIÉ solicita al usuario que actualice sus datos cada vez que éstos sufran alguna modificación, ya que esto permitirá brindarle un servicio eficiente y personalizado.</p>
<h2>Transferencias de información con terceros</h2> 
<p>Nos comprometemos a no transferir su información personal a terceros, salvo las excepciones previstas en el artículo 37 de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, así como a realizar esta transferencia en los términos que fija la normatividad aplicable.</p>
<h2>Cambios en el aviso de privacidad</h2>
<p>Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, para la atención de novedades legislativas o jurisprudenciales, políticas internas, nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios o productos y prácticas del mercado. Estas modificaciones estarán disponibles al público a través de nuestra página de Internet www.olie.com.mx, sección aviso de privacidad.</p>
<h2>Aceptación de los términos</h2>
<p>Esta declaración de Privacidad está sujeta a los términos y condiciones del sitio web de OLIÉ antes descrito, lo cual constituye un acuerdo legal entre el usuario y OLIÉ. Si el usuario utiliza los servicios en el sitio de OLIÉ, significa que ha leído, entendido y acordado los términos antes expuestos.</p>
<h2>Autoridad</h2>
<p>Si el usuario considera que han sido vulnerados sus derechos respecto de la protección de datos personales, tiene el derecho de acudir a la autoridad correspondiente para defender su ejercicio. La autoridad es el Instituto Federal de Acceso a la Información y Protección de Datos (IFAI), su sitio web es: www.ifai.mx.</p>
<h2>Fecha de la última actualización</h2>
<p>Estimado usuario, se le notifica que el Aviso de Privacidad ha sido modificado el 1 de marzo del 2021</p>
<h2>Qué son los cookies y cómo se utilizan</h2>
<p>Los cookies son pequeñas piezas de información que son enviadas por el sitio Web a su navegador y se almacenan en el disco duro de su equipo y se utilizan para determinar sus preferencias cuando se conecta a los servicios de nuestros sitios, así como para rastrear determinados comportamientos o actividades llevadas a cabo por usted dentro de nuestros sitios. En algunas secciones de nuestro sitio requerimos que el cliente tenga habilitados los cookies ya que algunas de las funcionalidades requieren de éstas para trabajar. Los cookies nos permiten: a) reconocerlo al momento de entrar a nuestros sitios y ofrecerle de una experiencia personalizada, b) conocer la configuración personal del sitio especificada por usted, por ejemplo, los cookies nos permiten detectar el ancho de banda que usted ha seleccionado al momento de ingresar al home page de nuestros sitios, de tal forma que sabemos qué tipo de información es aconsejable descargar, c) calcular el tamaño de nuestra audiencia y medir algunos parámetros de tráfico, pues cada navegador que obtiene acceso a nuestros sitios adquiere un cookie que se usa para determinar la frecuencia de uso y las secciones de los sitios visitadas, reflejando así sus hábitos y preferencias, información que nos es útil para mejorar el contenido, los titulares y las promociones para los usuarios. Los cookies también nos ayudan a rastrear algunas actividades, por ejemplo, en algunas de las encuestas que lanzamos en línea, podemos utilizar cookies para detectar si el usuario ya ha llenado la encuesta y evitar desplegarla nuevamente, en caso de que lo haya hecho. Sin embargo, las cookies le permitirán tomar ventaja de las características más benéficas que le ofrecemos, por lo que le recomendamos que las deje activadas. La utilización de cookies no será utilizada para identificar a los usuarios, con excepción de los casos en que se investiguen posibles actividades fraudulentas.</p>
<h2>Tecnología Clickstream</h2>
<p>Nuestro servidor Web recoge automáticamente información clickstream tal como la dirección (o URL) del sitio Web del que procede antes de visitar nuestro sitio, qué páginas visita de nuestro sitio, qué buscador usa para visitar nuestro sitio, así como todos los términos de búsqueda que pueda haber ingresado en nuestro sitio, entre otras cosas. Nuestro sitio Web también puede usar otras tecnologías para obtener información sobre las páginas a las que acceden nuestros visitantes. Esta información anónima de clickstream ofrece a nuestros clientes una mejor experiencia de compra y nos ayuda a comprender la forma en que los visitantes usan nuestro sitio Web. En el caso de empleo de cookies, el botón de “ayuda” que se encuentra en la barra de herramientas de la mayoría de los navegadores, le dirá cómo evitar aceptar nuevos cookies, cómo hacer que el navegador le notifique cuando recibe un nuevo cookie o cómo deshabilitar todos los cookies.</p>
<h2>Uso de la información personal en CRM</h2>
<p>Utilizamos la información personal que recopilamos a través del CRM para los fines comerciales de nuestra empresa, que incluyen, entre otros, proporcionar y mantener el CRM, procesar transacciones y pagos, responder a consultas y solicitudes, ofrecer soporte técnico y personalizar la experiencia del usuario. También podemos utilizar la información personal para fines de marketing y publicidad, como enviar correos electrónicos y mensajes de texto promocionales y ofrecer productos y servicios relacionados con nuestros productos y servicios actuales.</p>
<h2>Divulgación de la información personal en CRM </h2>
<p>Podemos divulgar la información personal que recopilamos a través del CRM a terceros, como proveedores de servicios externos que utilizamos para proporcionar el CRM, proveedores de servicios de pago y otros proveedores de servicios que utilizamos para mejorar nuestra empresa y nuestros productos y servicios. También podemos divulgar información personal si estamos obligados a hacerlo por ley o si creemos de buena fe que la divulgación es necesaria para proteger nuestros derechos, propiedad o seguridad, o la de nuestros usuarios o terceros.</p>
<h2>Seguridad de la información personal en CRM</h2>
<p>Tomamos medidas razonables para proteger la información personal que recopilamos a través del CRM contra el acceso no autorizado, la divulgación, la alteración o destrucción. Sin embargo, ninguna medida de seguridad es completamente segura y no podemos garantizar la seguridad absoluta de la información personal.
Derechos de privacidad de los usuarios Los usuarios tienen ciertos derechos de privacidad con respecto a su información personal, como el derecho a acceder, corregir, eliminar y objetar el uso de su información personal. Para ejercer estos derechos, los usuarios pueden ponerse en contacto con nosotros utilizando la información de contacto que se proporciona al final de este Aviso de Privacidad.</p>
<h2>Cambios a este Aviso de Privacidad en CRM</h2>
<p>Nos reservamos el derecho de modificar este Aviso de Privacidad en cualquier momento. Cualquier cambio entrará en vigor a partir de la fecha de publicación de la versión actualizada en nuestro sitio web o en el CRM. Se recomienda a los usuarios que revisen este Aviso de Privacidad periódicamente para estar al tanto de cualquier cambio. Si no está de acuerdo con los términos de este Aviso de Privacidad, debe dejar de utilizar el CRM.</p>
<h2>Contacto</h2>
<p>Si tiene alguna pregunta o inquietud sobre este Aviso de Privacidad o sobre nuestras prácticas de privacidad en relación con el CRM, puede comunicarse con nosotros a través de: hola@olie.mx.</p>
    
</main>
@endsection