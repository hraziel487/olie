@extends('layouts.app2', ['class' => 'g-sidenav-show bg-gray-100'])

@section('content')
@include('layouts.navbars.auth.topnav2', ['title' => 'Dashboard'])
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
      <div class="card">
        <div class="card-body p-3">
          <div class="row">
            <div class="col-8">
              <div class="numbers">
                <p class="text-sm mb-0 text-uppercase font-weight-bold">Visitas Acumuladas</p>
                <h5 class="font-weight-bolder">
                  1
                </h5>
                <p class="mb-0">
                  <span class="text-success text-sm font-weight-bolder">+55%</span>
                  mas que ayer
                </p>
              </div>
            </div>
            <div class="col-4 text-end">
              <div class="icon icon-shape bg-gradient-primary shadow-primary text-center rounded-circle">
                <i class="ni ni-money-coins text-lg opacity-10" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
      <div class="card">
        <div class="card-body p-3">
          <div class="row">
            <div class="col-8">
              <div class="numbers">
                <p class="text-sm mb-0 text-uppercase font-weight-bold">Premios obtenidos</p>
                <h5 class="font-weight-bolder">
                  2
                </h5>
                <p class="mb-0">
                  <span class="text-success text-sm font-weight-bolder">+3%</span>
                  mas que ayer
                </p>
              </div>
            </div>
            <div class="col-4 text-end">
              <div class="icon icon-shape bg-gradient-danger shadow-danger text-center rounded-circle">
                <i class="ni ni-world text-lg opacity-10" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-xl-0 mb-4">
      <div class="card">
        <div class="card-body p-3">
          <div class="row">
            <div class="col-8">
              <div class="numbers">
                <p class="text-sm mb-0 text-uppercase font-weight-bold">Novedades Olie</p>
                <h5 class="font-weight-bolder">
                  +3
                </h5>
                <p class="mb-0">
                  <span class="text-danger text-sm font-weight-bolder">-2%</span>
                  mas que ayer
                </p>
              </div>
            </div>
            <div class="col-4 text-end">
              <div class="icon icon-shape bg-gradient-success shadow-success text-center rounded-circle">
                <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-sm-6">
      <div class="card">
        <div class="card-body p-3">
          <div class="row">
            <div class="col-8">
              <div class="numbers">
                <p class="text-sm mb-0 text-uppercase font-weight-bold">Nuevos Productos</p>
                <h5 class="font-weight-bolder">
                  +10
                </h5>
                <p class="mb-0">
                  <span class="text-success text-sm font-weight-bolder"></span>
                </p>
              </div>
            </div>
            <div class="col-4 text-end">
              <div class="icon icon-shape bg-gradient-warning shadow-warning text-center rounded-circle">
                <i class="ni ni-cart text-lg opacity-10" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col-lg-12 mb-lg-0 mb-4">
      <div class="card z-index-2 h-100">
        <div class="card-header pb-0 pt-3 bg-transparent">
          <h6 class="text-capitalize">Resumen De Visitas</h6>
          <p class="text-sm mb-0">
            <i class="fa fa-arrow-up text-success"></i>
            <span class="font-weight-bold">{{$visita->Numerovisita}}% mas</span> en 2023
          </p>
        </div>
        <div class="card-body p-3">
          <div class="chart">
            <div class="progress-wrapper">
              <div class="progress-info">
                <div class="progress-percentage">
                @if($visita->Numerovisita == "1")
                  <span class="text-sm font-weight-bold">30%</span>
                  @elseif($visita->Numerovisita == "2")
                  <span class="text-sm font-weight-bold">60%</span>
                  @elseif($visita->Numerovisita == "3")
                  <span class="text-sm font-weight-bold">100%</span>
                  @else
                  <span class="text-sm font-weight-bold">0%</span>
                  @endif
                </div>
              </div>
              <div class="progress">
              @if($visita->Numerovisita == "1")
                <div class="progress-bar bg-gradient-primary" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%;"></div>
                @elseif($visita->Numerovisita == "2")
                <div class="progress-bar bg-gradient-primary" role="progressbar" aria-valuenow="66" aria-valuemin="0" aria-valuemax="100" style="width: 66%;"></div>
                @elseif($visita->Numerovisita == "3")
                <div class="progress-bar bg-gradient-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                @else
                <div class="progress-bar bg-gradient-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                @endif
              </div>
            </div>
          </div>
        
        </div>
        
      </div>
    
    </div>
    <div class="col-md-12  ">
    <button type="button" class="btn btn-block bg-gradient-warning mb-3 float-end mt-4" data-bs-toggle="modal" data-bs-target="#modal-notification">Registrar Visita</button>
    <div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
      <div class="modal-dialog modal-danger modal-dialog-centered modal-" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h6 class="modal-title" id="modal-title-notification">Tu atencion es requerida</h6>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="py-3 text-center">
            <div class="card card-plain shadow-none" id="sidenavCard">
            <img class="w-25 mx-auto" src="{{asset('img/logo.png') }}"
                alt="sidebar_illustration">
        </div>
              <div class="card card-plain">
              <div class="card-header pb-0 text-left">
                <h3 class="mb-0 warning ">Bienvenido de vuelta</h3>
              </div>
              <div class="card-body">
              <div class="row g-2" id="form-pass">
        <div class="col-auto">
        <label for="staticPassword" class="">Poner contraseña</label>
        <input type="password" class="input-group mb-3"  id="contrasena" />
        </div>
        <div class="col-auto">
        <input type="button" class="btn btn-primary mt-4"  value="Validar" onclick="validarContrasena()"/>
        </div>
    </div>
              <form method="post" action="{{ route('visitas.update',$visita->id ) }}" autocomplete="off" id="formulario" style="display:none;" enctype="multipart/form-data">
              @csrf
                            @method('put')

   
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                        <label class="form-control-label d-none" for="input-status">{{ __('user_id') }}</label>
                    
                        <input type="text" name="user_id" id="user_id" class="form-control d-none form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Registra tu visita') }}" value="{{Auth::user()->id}}" required>
                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('Nombre') }}</label>
                        <input type="number" name="Numerovisita" id="input-status" class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Numero de visita') }}" value="{{$visita->Numerovisita}}" min="1" max="3" required>

                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-white" data-bs-dismiss="modal">Cerrar</button>
        <button type="submit"  class="btn btn-white">Registrar Visita</button>
        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

</div>
@include('layouts.footers.auth.footer')
</div>
@endsection

@push('js')
<script>
  function validarContrasena() {
    var contrasena = document.getElementById('contrasena').value;
    if (contrasena == '12345') {
      
      alert('Contraseña Correcta')
      document.getElementById('form-pass').style.display = 'none';
      document.getElementById('formulario').style.display = 'block';
    } else {
      alert('Contraseña incorrecta');
    }
  }
</script>
@endpush