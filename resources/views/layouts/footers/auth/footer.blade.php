<div class="justify-content-center d-flex">
    <!-- Facebook -->
    <a style="margin-right: 5%;" href="#!" role="button"><img src="{{asset('img/face.svg') }}" alt=""></a>

    <!-- Instagram -->
    <a  href="#!" role="button"><img src="{{asset('img/insta.svg') }}" alt=""></a>
</div>
    <footer class="footer ">
    <div class="mt-5 mb-2" style="background-color: black; color:white;">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-lg-6 mb-lg-0 mb-4">


                <div class="copyright text-center text-sm text-muted text-lg-start mt-4" style="color:white !important ">
                    OLIÉ 2023
                </div>
            </div>

        </div>
    </div>
</footer>