<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Visita;

class HomeController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $visita = Visita::where('user_id', auth()->user()->id)->get();
        return view('pages.dashboard', compact('visita'));
    }
    public function edit($id)
    {
        $visita = Visita::findOrFail($id);
        return view('pages.EditVisitCounter', compact('visita'));
    }

}
