<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Visita;
use App\Models\User;

class VisitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function navindex()
    {
        $visita = Visita::where('user_id', auth()->user()->id)->get();
        $usuario = User::all();
        return view('pages.VisitCounter', compact('visita','usuario'));
    }
    public function index()
    {
        $visita = Visita::where('user_id', auth()->user()->id)->get();

        return view('pages.VisitCounter', compact('visita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $visita = Visita::create($request->all());
        $visita->save();
        return redirect()->route('visitas')->with('flash','Su visita ha sido guardada satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function edit($id)
    {
        $visita = Visita::findOrFail($id);
        return view('pages.EditVisitCounter', compact('visita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $visita = Visita::findOrFail($id);
        $visita->fill($request->all());
        $visita->save();
        return redirect()->route('visitas')->with('flash','Su visita ha sido guardada satisfactoriamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
